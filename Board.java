public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];
	}
	
	public boolean playATurn(){
		boolean status;
		System.out.println(); //line for visual purposes
		this.die1.roll();
		this.die2.roll();
		
		System.out.println(this.die1);
		System.out.println(this.die2);
		
		int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
		
		if(!(this.tiles[sumOfDice-1])){
			this.tiles[sumOfDice-1] = true; //makes the tile equal to sum true if its false
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			status = false;
		}
		else{
			if(!(this.tiles[this.die1.getFaceValue()-1])){
				this.tiles[this.die1.getFaceValue()-1] = true; //makes the tile equal to the face value of the first die true if its false
				System.out.println("Closing tile with the same value as die one: " + this.die1.getFaceValue());
				status = false;
			}
			else{
				if(!(this.tiles[this.die2.getFaceValue()-1])){
					this.tiles[this.die2.getFaceValue()-1] = true; //makes the tile equal to the face value of the second die true if its false
					System.out.println("Closing tile with the same value as die two: " + this.die2.getFaceValue());
					status = false;
				}
				else{
					status = true;
				}
			}
		}
		return status;
	}
	
	public String toString(){
		String state ="";
		
		for(int i=0; i<tiles.length; i++){
			if(tiles[i]){
				state+= "X ";
			}
			else{
				state += (i+1) + " ";
			}
		}
		return "tiles = {" + state + "}";
	}
}